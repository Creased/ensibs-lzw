#####################################################
######  Introduction à la cryptographie  	###
#####   Codes de Huffman             		###
####################################################

import copy
from collections import defaultdict
from heapq import *
from io import BytesIO

###  distribution de proba sur les letrres

def frequences(file) :
    str1 = open(file,"r").read()
    table = {}

    for n in str1:
        if n in table.keys():
            table[n] += 1
        else:
            table[n] = 1

    return table

#F = frequences()
#print(F)

###  la classe Arbre

class Arbre :
    def __init__(self, lettre, gauche=None, droit=None):
        self.gauche=gauche
        self.droit=droit
        self.lettre=lettre
    def estFeuille(self):
        return self.gauche == None and self.droit == None
    def estVide(self):
        return self == None
    def __cmp__(self,x):
        if(x == None):
            return -1
        else:
            return 1
    def __lt__(self, other):
        if(other ==  None):
            return -1
        else:
            return self < other


    def __str__(self):
        return '<'+ str(self.lettre)+'.'+str(self.gauche)+'.'+str(self.droit)+'>'


###  Ex.1  construction de l'arbre d'Huffamn utilisant la structure de "tas binaire"
def arbre_huffman(frequences) :
    tas = [(freq, char, None) for (char, freq) in frequences.items()]
    heapify(tas)
    i=0
    while len(tas) >= 2 :
        d = heappop(tas)
        g = heappop(tas)
        fd,ed,ad = d
        fg,eg,ag = g
        arbre = Arbre(chr(97+i),g,d)
        etiquette = (fg+fd,chr(97+i),arbre)
        i = i+1
        heappush(tas,etiquette)
    return heappop(tas)

###  Ex.2  construction du code d'Huffamn
def parcours(arbre,prefixe,code) :
    p,e,a = arbre
    if( a == None ):
        code[e] = prefixe
    else :
        parcours(a.gauche, prefixe+'0', code)
        parcours(a.droit, prefixe+'1', code)

def code_huffman(arbre) :
    # on remplit le dictionnaire du code d'Huffman en parcourant l'arbre
    code = {}
    parcours(arbre,'',code)
    return code




###  Ex.3  encodage d'un texte contenu dans un fichier

def encodage(dico,fichier) :
    fichier = open(fichier,'r')
    texte = fichier.read()

    coded = ""
    out = ""
    for i in texte :
            coded += dico[i]
    print("[+] La taille du fichier non compressé est de : "+str(len(coded)))
    for i in range(0,len(coded),4):
        out+=chr(int(coded[i:i+4],2))
    out=chr(int(len(out)%4))+out
    print("[+] Compression effectuée")
    return out

###  Ex.4  décodage d'un fichier compresse

def decodage(arbre,fichierCompresse) :
    chaine = [ ord(i) for i in fichierCompresse]
    out = ""
    for i in range(1,len(chaine)-1) :
        bina =bin(chaine[i])[2:]
        out += "0"*(4-len(bina))+bina
    bina =bin(chaine[len(chaine)-1])[2:]
    out+="0"*(chaine[0] - len(bina))+bina

    def searchChar(arbre,binaire):
        p,e,a = arbre
        if(a!=None and len(binaire)!=0 ):
            if(binaire[0] == "0"):
                return searchChar(a.gauche,binaire[1:])
            else:
                return searchChar(a.droit,binaire[1:])
        else:
            return (e,binaire)

    decoded = ""
    while(len(out)!=0):
        char,bina = searchChar(arbre,out)
        out = bina
        decoded+=char
    print(decoded)


def process(file):
    freq = frequences(file)
    arbre = arbre_huffman(freq)
    code = code_huffman(arbre)
    compressed = encodage(code,file)
    print("[+] Taille compressé :",len(compressed))
    print("[+] Décompression effectuée")
    print("[+] Texte décompressé :\n\n"+'='*120)
    decodage(arbre,compressed)
