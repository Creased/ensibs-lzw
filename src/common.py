#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os
import numpy

def check_file_exists(file_path):
	"""Check if file exists."""
	return os.path.isfile(file_path)

def zfill_bin(binary, max_value):
	"""Fill binary string with 0 based on integer max_value."""
	if binary.startswith('0b'):
		binary = binary[2:]

	return binary.zfill(int(numpy.log(max_value) / numpy.log(2)))