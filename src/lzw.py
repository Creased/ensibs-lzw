#!/usr/bin/env python3
# -*- coding:utf-8 -*-

"""TP LZW Python."""

# Generate documentation: epydoc -v --html lzw.py -o ./docs
# Make tests: python3 -m doctest -v lzw.py

import io
import os
from .common import check_file_exists

class LZW(object):
    """Perform LZW compression."""
    def __init__(self, input_file_path, output_file_path, overwrite):
        if not check_file_exists(input_file_path):
            print('Error: input file doesn\'t exists!')
            exit(0)
        elif check_file_exists(output_file_path) and not overwrite:
            print('Error: output file already exists!')
            exit(0)
        else:
            self.input = input_file_path
            self.output = output_file_path

    def compress(self, dict_size=256):
        dictionary = [chr(i) for i in range(0, dict_size)]
        compression_blob = []
        previous = ''
        with io.open(self.input, 'r', encoding='utf-8') as file_desc:
            lines = file_desc.read()
            for current in list(lines.strip()):
                pre_cur = ('{}{}'.format(previous, current))

                if pre_cur in dictionary:
                    previous = pre_cur
                else:
                    compression_blob.append(chr(dictionary.index(previous)))
                    dictionary.append(pre_cur)
                    previous = current

            if previous:
                compression_blob.append(chr(dictionary.index(previous)))

        with io.open(self.output, 'w', encoding='utf-8') as file_desc:
            file_desc.write(''.join(compression_blob))

        statinfo = {'input': os.stat(self.input), 'output': os.stat(self.output)}
        print('Compression succeeded, ratio: {}'.format(statinfo['input'].st_size / statinfo['output'].st_size))

    def decompress(self, dict_size=256):
        dictionary = [chr(i) for i in range(0, dict_size)]
        decompression_blob = []
        previous = ''
        with io.open(self.input, 'r', encoding='utf-8') as file_desc:
            lines = file_desc.read()
            for current in list(lines.strip()):
                pre_cur = ('{}{}'.format(previous, dictionary[ord(current)][0]))

                if pre_cur in dictionary:
                    previous = dictionary[ord(current)]
                else:
                    previous = dictionary.append(pre_cur)
                    previous = dictionary[ord(current)]

                decompression_blob.append(dictionary[ord(current)])

        with io.open(self.output, 'w', encoding='utf-8') as file_desc:
            file_desc.write(''.join(decompression_blob))
