#!/usr/bin/env python3
# -*- coding:utf-8 -*-

"""TP LZW Python."""

# Generate documentation: epydoc -v --html main.py -o ./docs
# Make tests: python3 -m doctest -v main.py

import time
import argparse
import traceback

from src.lzw import LZW
import src.huffman as huffman

__author__ = 'Baptiste MOINE <contact@bmoine.fr>, Romain KRAFT <romain.kraft@protonmail.com>'
__version__ = '0.1-dev'
__date__ = '30 April 2018'

DEBUG = False

def parse_args():
    """Arguments parsing."""
    parser = argparse.ArgumentParser(
        description='Compression toolkit v{version}'.format(
            version=__version__
        )
    )

    parser.add_argument('-a', '--algorithm',
                        type=str,
                        default='lzw',
                        help='algorithm to be used for compression')
    parser.add_argument('-d', '--decompress',
                        action='store_true',
                        default=False,
                        help='decompression mode')
    parser.add_argument('-i', '--input',
                        type=str,
                        required=True,
                        default='demo_file',
                        help='input file')
    parser.add_argument('-o', '--output',
                        type=str,
                        required=False,
                        default='demo_file.out',
                        help='output file')
    parser.add_argument('-f', '--force',
                        action='store_true',
                        default=False,
                        help='overwrite output file')
    parser.add_argument('-D', '--debug',
                        action='store_true',
                        default=False,
                        help='debug')

    args = parser.parse_args()

    return args

def main():
    """Main function."""
    try:
        # Arguments parsing
        args = parse_args()

        # Overwrite DEBUG with user value (bool)
        global DEBUG
        DEBUG = args.debug
        if DEBUG: print('Debug mode set')

        start_time = time.time()

        # Process compression
        if args.algorithm.lower() == 'lzw':
            print('Use LZW...')
            lzw = LZW(args.input, args.output, args.force)
            if args.decompress:
                lzw.decompress()
            else:
                lzw.compress()
        elif args.algorithm.lower() == 'huffman':
            print('Use Huffman...')
            huffman.process(args.input)
        else:
            print('No such algorithm has been implemented yet...')
        # Print duration time
        end_time = time.time() - start_time
        print('Duration: {} seconds'.format(round(end_time, 4)))
    except (ValueError, TypeError, OSError) as exception_:
        print(traceback.format_exc())
        print(exception_)

# Runtime processor
if __name__ == '__main__':
    main()
