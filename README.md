# TP Python LZW ENSIBS

L'objectif de ce TP est d'implémenter un algorithme de compression en utilisant
le langage de Python.

## Énoncé

Développer un programme Python permettant de réaliser la compression et la
décompression de fichier texte via l'algorithme LZW.
Si le temps vous le permet, enrichir votre logiciel
(autre algorithme de compression, GUI, fonctionnalité spécifique de votre choix).

## Installation

⚠ Ces scripts sont développés pour correspondre au maximum aux nouvelles exigences de
Python 3. Aussi, afin d'exploiter au mieux ces différents scripts, il est nécessaire de
procéder à l'installation de Python 3 et des différents modules&nbsp;:

```bash
apt-get install python3 python3-pip
pip3 install -r requirements.txt
```

## Demo

```bash
python3 main.py -a LZW -i demo_file -o demo_file.out -f
```

## Documentation

L'ensemble de la documentation est générée au moyen de Epydoc et accessible depuis
le répertoire [docs/](docs) (voir le fichier [docs/index.html](docs/index.html)
pour accéder à la table des matières).
